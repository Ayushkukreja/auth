<?php
require('./app/init.php');
$errors = $validator->errors();
// require_once 'app/classes/Database.php';
// require_once 'app/classes/ErrorHandler.php';
// require_once 'app/classes/Validator.php';

if(!empty($_POST))
{	
	
	// $database = new Database();
	// $errorHandler = new ErrorHandler();
	// $validator = new Validator($database, $errorHandler);
	$validator->validate($_POST, [
		'email' => [
			'required' => true,
			'maxlength' => 200,
			'email' => true,
			'unique' => 'users.email'
		],
		'username' => [
			'required' => true,
			'maxlength' => 255,
			'minlength' => 3,
			'unique' => 'users.username'
		],
		'password' => [
			'required' => true,
			'minlength' => 8
		]
	]);
	if(!$validator->fails()){
		$created = $user->create($_POST);
		if($created){
			header("Location: index.php");
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
<h1>Sign up</h1>
	<form action="signup.php" method="POST">
		<fieldset>
			<legend>Sign Up</legend>
			<br>
			<label>Email:
				<input type="email" name="email">
				<?php 
				if($errors->has('email'))
					echo "<span style='color:red; font-size:12px'>{$errors->first('email')}</span>";
				?>
			</label>
			<br><br>
			
			<label>Username: 
				<input type="text" name="username">
				<?php 
				if($errors->has('username'))
					echo "<span style='color:red; font-size:12px'>{$errors->first('username')}</span>";
				?>
			</label>
			<br><br>

			<label>Password: 
				<input type="password" name="password">
				<?php 
				if($errors->has('password'))
					echo "<span style='color:red; font-size:12px'>{$errors->first('password')}</span>";
				?>
			</label>
			<br><br>

			<input type="submit" value="Sign Up">

		</fieldset>
	</form>
</body>
</html>