<?php
require('./app/init.php');
if(isset($_POST['login'])){
    $loggedIn = $user->signIn($_POST);
    $rememberMe = isset($_POST['remember']);
    if($loggedIn) {
        if($rememberMe){
            $userId = $_SESSION[User::$sessionKey];
            $tokenDate = $token->createRememberMeToken($userId);
            setcookie("remember", $tokenDate['token'],time()+Token::$REMEMBER_ME_EXPIRY_TIME_FOR_COOKIE);
        }
        header('Location: index.php');
    }
    else
    {
        echo "Error in username/password";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <form action="login.php" method="POST">
        <div>
            <label for="username">Username</label>
            <input type="text" name="username" placeholder="Username or Registered Email">
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" name="password" placeholder="Password">
        </div>
        <div>
            <input type="checkbox" name="remember" value="1">
            <label for="remember">remember me</label>
        </div>
        <div>
            <input type="submit" name="login" value="Login">
        </div>
    </form>
</body>
</html>