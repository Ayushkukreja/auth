<?php
session_start();

$app = __DIR__;

require_once "{$app}/classes/Util.php";
require_once "{$app}/classes/Token.php";
require_once "{$app}/classes/ErrorHandler.php";
require_once "{$app}/classes/Validator.php";
require_once "{$app}/classes/Hash.php";
require_once "{$app}/classes/User.php";
require_once "{$app}/classes/Database.php";

$database = new Database();
$token = new Token($database);
$user = new User($database);
$validator = new Validator($database);
$token->build();
$user->build();