<?php
class Database 
{
	protected string $host = 'localhost';
	protected string $database = 'auth2';
	protected string $username = 'Ayushkukreja';
	protected string $password = 'ayush';

	protected PDO $pdo;

	protected PDOStatement $ps;

	protected string $table;

	public function __construct()
	{
		$dsn = "mysql:host={$this->host};dbname={$this->database}";	
        $this->pdo = new PDO($dsn, $this->username, $this->password);
		$this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
	}
    public function table(string $table):Database {
        $this->table = $table;
        return $this;
    }
    public function where(string $field, string $operator, string $value):Database{
        $sql = "SELECT * FROM {$this->table} WHERE {$field} {$operator} :value";
        $this->ps = $this->pdo->prepare($sql);
		$this->ps->execute(['value'=>$value]);
		return $this;
    }
    public function get()
	{
		return $this->ps->fetchAll();
	}
	public function first()
	{
		return $this->get()[0];
	}
	public function insert(array $data)
	{
		$keys = array_keys($data); //['firstname', 'lastname']

		$fields = '`' . implode("`, `", $keys) . '`';
		$placeholders = ":" . implode(', :', $keys);

		$sql = "INSERT INTO {$this->table}({$fields}) VALUES ({$placeholders})";

		$this->ps = $this->pdo->prepare($sql);
		return $this->ps->execute($data);
	}
	public function count(): int
	{
		return $this->ps->rowCount();
	}
	public function deleteWhere($field, $value)
	{
		$sql = "DELETE FROM {$this->table} WHERE {$field} = :value";
		$this->ps = $this->pdo->prepare($sql);
		$this->ps->execute(['value'=>$value]);
		return $this;
	}
	
	public function exists($field,$value): bool
	{
		return $this->where($field, '=',$value)->count() ? true :false;
	}
	public function raw(string $query)
	{
		$this->pdo->query($query);
	}
	public function orWhere(array $data){
		$keys = array_keys($data);
		$condition = "";

		foreach($keys as $index=>$key)
		{
			
			if($index != 0)
			{
				$condition .= " OR ";
			}
			$condition .= "{$key} = :$key";
		}
		$sql = "SELECT * FROM {$this->table} WHERE $condition";
		$this->ps = $this->pdo->prepare($sql);
		$this->ps->execute($data);
		return $this;
	}
	public function fetchAll(string $query, int $fetchMode = PDO::FETCH_ASSOC)
	{
		return $this->pdo->query($query)->fetchAll($fetchMode);
	}
}
?>