<?php

class Token
{
    private Database $database;
	private static $REMEMBER_ME_EXPIRY_TIME = '30 minutes';
    public static $REMEMBER_ME_EXPIRY_TIME_FOR_COOKIE = 1800;

	protected static $FORGOT_PWD_EXPIRY_TIME = '10 minutes';

	private string $table = 'tokens';

	private const CREATE_QUERY = "CREATE TABLE IF NOT EXISTS tokens (id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT, user_id INT UNSIGNED, token VARCHAR(255) UNIQUE, expires_at DATETIME NOT NULL, is_remember TINYINT DEFAULT 0)";
	public function __construct(Database $database)
	{
		$this->database = $database;
	}

	public function build()
	{
		$this->database->raw(Token::CREATE_QUERY);
	}

    public function createRememberMeToken(int $userId):array|null
    {
        return $this->createToken($userId, 1);
    }
    public function createForgotPasswordToken(int $userId):array|null
    {
        return $this->createToken($userId, 0);
    }
	public function getValidExistingToken(int $userId, int $isRemember): array|null
	{
        $currentDateTime = date('y-m-d H:i:s');
		$sql = "SELECT * FROM {$this->table} WHERE user_id = {$userId} AND is_remember = {$isRemember} AND expires_at >= '{$currentDateTime}'";
		$token = $this->database->fetchAll($sql);
        if($token){
            return $token[0];
        }
		return null;
	}
    protected function createToken(int $userId, int $isRemember): array|null
    {
        $validToken = $this->getValidExistingToken($userId, $isRemember);
        if($validToken != null){
            return $validToken;
        }

        $currentTime = date('Y-m-d H:i:s');
        $timeToBeAdded = $isRemember ? self::$REMEMBER_ME_EXPIRY_TIME : self::$FORGOT_PWD_EXPIRY_TIME;

        $token = Hash::generateToken($userId);
        $expires_at = date('y-m-d H:i:s',strtotime($currentTime . '+'. $timeToBeAdded));

        $data = [
            'user_id' => $userId,
            'token' => $token,
            'expires_at' => $expires_at,
            'is_remember' => $isRemember
        ];

        return $this->database
                    ->table($this->table)
                    ->insert($data)? $data : null;
    }
    public function verify(string $token,int $isRemember)
    {
        $currentDateTime = date('y-m-d H:i:s');
		$sql = "SELECT * FROM {$this->table} WHERE token = '{$token}' AND is_remember = {$isRemember} AND expires_at >= '{$currentDateTime}'";
		$token = $this->database->fetchAll($sql);
        if($token){
            return $token[0];
        }
		return null;
    }
    public function delete(string $userId, int $isRemember)
    {
        $sql = "DELETE FROM {$this->table} WHERE user_id = {$userId} AND is_remember = {$isRemember}";
        $this->database->raw($sql);
    }
}

?>