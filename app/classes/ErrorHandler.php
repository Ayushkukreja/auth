<?php
class ErrorHandler
{
    public $errors = [];
    public function addError($error,$key){
        $this->errors[$key][] = $error;
    }
    public function hasErrors(): bool
	{
		return count($this->errors) > 0;
	}

	public function has($key): bool
	{
		return isset($this->errors[$key]);
	}

    public function first(string $key):string|null
    {
     if ($this->has($key)){
         return $this->errors[$key][0];
     }   
     return NULL;
    //  return $this->errors[$key]??NULL;
    }
	public function all(string $key = null):array|null
	{
        if($key == null){
          return $this->errors[$key];  
        }
        return $this->errors[$key] ?? null;
	}

}