<?Php
class User
{
    protected Database $database;
	
	protected string $table = 'users';
	public static $sessionKey = 'user';
    public function __construct(Database $database)
    {
        $this->database = $database;
    }
    public function build()
	{
		$sql = "CREATE TABLE IF NOT EXISTS {$this->table} (id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, email varchar(255) NOT NULL UNIQUE, username VARCHAR(255) NOT NULL UNIQUE, password VARCHAR(255) NOT NULL)";
        $this->database->raw($sql);
	}
    public function create($data)
	{
		if(isset($data['password'])) {
			$data['password'] = Hash::make($data['password']);
		}
		return $this->database
					->table($this->table)
					->insert($data);
	}
    public function signOut()
    {
        unset($_SESSION[self::$sessionKey]);
    }
    public function signIn(array $data):bool
    {
        $username = $data['username'];
        $password = $data['password'];
        $user = $this->database
                    ->table($this->table)
                    ->orWhere(['username'=>$data['username'], 'email'=>$data['username']]);

        if($user->count()) {
            $user = $user->first();

            if(HASH::verify($password, $user->password)) {
            // SET the session
                $this->setAuthSession($user->id);
                return true;
            }
            return false;
        }
        return false;
    }
    public function setAuthSession(int $id){
        $_SESSION[self::$sessionKey] = $id;
    }
    public function check(): bool{
        return isset($_SESSION[self::$sessionKey]);
    }

}
?>